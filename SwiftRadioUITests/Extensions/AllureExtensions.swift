//
//  AllureExtensions.swift
//  SwiftRadioUITests
//
//  Created by Борис Лысиков on 17.10.2020.
//  Copyright © 2020 matthewfecher.com. All rights reserved.
//

import XCTest

public extension String {
    static let allureName = "allure.name"
    static let allureLabel = "allure.label."
    static let component = "component"
    static let epic = "epic"
    static let suite = "suite"
    static let story = "story"
    static let feature = "feature"
    static let allureId = "AS_ID"
}

public extension XCTest {

    @discardableResult
    func component(_ value: String) -> XCTest {
        label(.component, value)
        return self
    }

    @discardableResult
    func epic(_ value: String) -> XCTest {
        label(.epic, value)
        return self
    }

    @discardableResult
    func feature(_ value: String) -> XCTest {
        label(.feature, value)
        label(.story, value)
        return self
    }

    @discardableResult
    func suite(_ value: String) -> XCTest {
        label(.suite, value)
        return self
    }

    @discardableResult
    func allureId(_ value: String) -> XCTest {
        label(.allureId, value)
        return self
    }

    @discardableResult
    func label(_ name: String, _ value: String) -> XCTest {
        XCTContext.runActivity(named: .allureLabel + name + ":" + value, block: {_ in})
        return self
    }

    @discardableResult
    func name(_ value: String) -> XCTest {
        XCTContext.runActivity(named: .allureName + ":" + value, block: {_ in})
        return self
    }

    @discardableResult
    func step(_ name: String, step: () -> Void) -> XCTest {
        XCTContext.runActivity(named: name) { _ in
            step()
        }
        return self
    }

}
