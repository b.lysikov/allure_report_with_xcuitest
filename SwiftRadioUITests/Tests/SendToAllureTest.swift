//
//  SendToAllureTest.swift
//  SwiftRadioUITests
//
//  Created by Борис Лысиков on 01.11.2020.
//  Copyright © 2020 matthewfecher.com. All rights reserved.
//

final class SendToAllureTest: CommonTest {
    
    private let mainPage = MainPage()
    private let stationDetailsPage = StationDetailsPage()
    
    /*
     Пример отправки отчета в allure с привязкой к id тест кейса
     */
    func testDisplayNowPlayingButton() {
        allureId("10041")
        
        step("Кнопка play не отображается") {
            mainPage
                .nowPlayingButtonIsExist(isExist: false)
        }
        step("Выбрать любую станцию"){
            mainPage
                .tapOnStation(index: 0)
        }
        step("Нажать кнопку назад") {
            stationDetailsPage
                .tapOnBackButton()
        }
        step("Кнопка play отображается") {
            mainPage
                .nowPlayingButtonIsExist(isExist: true)
        }
    }
    
    /*
     Пример отправки отчета в allure без привязки id
     */
    func testDisplayNowPlayingButton2() {
        component("Главный экран")
        feature("Отображение Play кнопки")
        
        step("Кнопка play не отображается") {
            mainPage
                .nowPlayingButtonIsExist(isExist: false)
        }
        step("Выбрать любую станцию"){
            mainPage
                .tapOnStation(index: 0)
        }
        step("Нажать кнопку назад") {
            stationDetailsPage
                .tapOnBackButton()
        }
        step("Кнопка play отображается") {
            mainPage
                .nowPlayingButtonIsExist(isExist: true)
        }
    }
    
}
