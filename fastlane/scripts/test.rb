require 'pathname'

CUSTOM_DERIVED_DATA_PATH = './DerivedData'.freeze

def convert_xcresults_to_json
  Dir.chdir('..')

  root = Pathname.pwd
  path_to_xcresult = Dir["#{CUSTOM_DERIVED_DATA_PATH}/Logs/Test/*.xcresult"].first
  allure_results = "allure-results"
  output = "#{root}/#{allure_results}"
  xcresult_script = "#{root}/scripts/xcresults"


  Dir.mkdir(allure_results) unless Dir.exist?(allure_results)

  puts 'Prepare xcresult contents for export to Allure'
  system("\"#{xcresult_script}\" export #{path_to_xcresult} #{output}")
end

def deleate_temporary_test_report_files
  # Remove all temporary files and directories
	puts "Remove all temporary files and directories"
  # File.delete(allure_results_file)
  path_to_xcresult = Dir["#{CUSTOM_DERIVED_DATA_PATH}/Logs/Test/*.xcresult"].first

	if File.exist("#{path_to_xcresult}")
		system("rm -rf #{path_to_xcresult}")
	else
		puts "Xcresults folder isn't exist"
	end
	if File.exist("#{output}")
		system("rm -rf #{output}")
	else
		puts "report isn't exis"
	end
end
